package com.colormatch.app;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fortyonepost.com.pwop.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class RecordVideo extends Activity implements SurfaceHolder.Callback {
	Bitmap picframe;
	byte[] pic;
	int pic_size;
	Button flash_bt , set_bt;
	MediaRecorder mediaRecorder;
	SurfaceHolder surfaceHolder;
	boolean isFlashOn = false;
	Camera mCamera;
	ImageView imageView;
	TextView  dominateColor;
	SharedPreferences pref;
	Editor editor;
	int width = 0, height = 0;
	private Camera.Size pictureSize;
	TextView red_vlaue , green_value , blue_value;
	ProgressBar bar_red, bar_blue , bar_green;
	boolean isSet = true;
	SurfaceHolder mySurfaceHolder;
	TextView red_txt , green_txt , blue_txt , percentage_txt;
	Button match_bt;	
	int rgb_new[];
	int rgb_old[];
	boolean isMatch = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		pref = getApplicationContext().getSharedPreferences("MyPref", 0);
		editor = pref.edit();
		mediaRecorder = new MediaRecorder();
		initMediaRecorder();

		setContentView(R.layout.video_layout);
		//colorName = (TextView) findViewById(R.id.colorName);
		dominateColor = (TextView) findViewById(R.id.dominateColor);
		//Hex = (TextView) findViewById(R.id.colorHex);
		// get the Image View at the main.xml file

		//
		red_vlaue = (TextView) findViewById(R.id.red_value);
		green_value = (TextView) findViewById(R.id.green_value);
		blue_value = (TextView) findViewById(R.id.blue_value);
		
		//
		bar_red = (ProgressBar) findViewById(R.id.progressbar_red);
		bar_green = (ProgressBar) findViewById(R.id.progressbar_green);
		bar_blue = (ProgressBar) findViewById(R.id.progressbar_blue);
		
		//
		
		//
		red_txt = (TextView) findViewById(R.id.txt_red);
		green_txt = (TextView) findViewById(R.id.txt_green);
		blue_txt = (TextView) findViewById(R.id.txt_blue);
		match_bt = (Button) findViewById(R.id.bt_match);
		red_txt.setVisibility(View.INVISIBLE);
		green_txt.setVisibility(View.INVISIBLE);
		blue_txt.setVisibility(View.INVISIBLE); 
		match_bt.setVisibility(View.INVISIBLE);
		//
		//
		percentage_txt = (TextView) findViewById(R.id.txt_percentage);
		//
		SurfaceView myVideoView = (SurfaceView) findViewById(R.id.videoview);
		surfaceHolder = myVideoView.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		flash_bt = (Button) findViewById(R.id.bt_flash);
		set_bt = (Button) findViewById(R.id.bt_set);

		imageView = (ImageView) findViewById(R.id.imageView_video);
		flash_bt.setOnClickListener(myButtonOnClickListener);
		
		set_bt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (isSet)
				{
					RecordVideo.this.runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							isSet = false;
							//
							red_txt.setVisibility(View.VISIBLE);
							green_txt.setVisibility(View.VISIBLE);
							blue_txt.setVisibility(View.VISIBLE); 
							match_bt.setVisibility(View.VISIBLE);
							
							red_txt.setText("R: "+ red_vlaue.getText());
							green_txt.setText("G: " + green_value.getText());
							blue_txt.setText("B: "+ blue_value.getText());
							rgb_old = new int [3];
							
							rgb_old[0] = Integer.parseInt(red_vlaue.getText().toString());
							rgb_old[1] = Integer.parseInt(green_value.getText().toString());
							rgb_old[2] = Integer.parseInt(blue_value.getText().toString());

							
									
									
							set_bt.setText("Unset");							
						}
					});
					
				}
				else
				{
					RecordVideo.this.runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							isSet = true;
							//
							red_txt.setVisibility(View.INVISIBLE);
							green_txt.setVisibility(View.INVISIBLE);
							blue_txt.setVisibility(View.INVISIBLE); 
							match_bt.setVisibility(View.INVISIBLE);
							//
							set_bt.setText("Set");
												
						}
					});
					
				}
				
				
			}
		});
		
		match_bt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				RecordVideo.this.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						if(isMatch)
							isMatch = false;
						else
							isMatch = true;
					}
				});
			}
		});
	}	

	private Button.OnClickListener myButtonOnClickListener = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {

			final Parameters p = mCamera.getParameters();

			// turn flash on
			if (isFlashOn) {
				Log.i("info", "torch is turned off!");
				p.setFlashMode(Parameters.FLASH_MODE_OFF);
				mCamera.setParameters(p);
				isFlashOn = false;
			} else {
				Log.i("info", "torch is turned on!");
				p.setFlashMode(Parameters.FLASH_MODE_TORCH);
				mCamera.setParameters(p);
				isFlashOn = true;
			}
		}
	};

	@Override
	public void surfaceChanged(SurfaceHolder holder, int arg1, int arg2,
			int arg3) {
            mySurfaceHolder = holder;
           /* GetCameraPreview cameraPreview = new GetCameraPreview();
            cameraPreview.execute();*/
			runMyAsynckTask();

            
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		mCamera = Camera.open();
		prepareMediaRecorder();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

	}

	//Run camera in background-task 
	class GetCameraPreview extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... arg0) {
			runMyAsynckTask();
			return null;
		}
		
	}
	
	private void runMyAsynckTask()
	{
		try {
			mCamera.stopPreview();
			mCamera.setDisplayOrientation(90);
			setBestPictureResolution();
			mCamera.setPreviewDisplay(mySurfaceHolder);
		} catch (IOException e) {
			e.printStackTrace();
		}

		mCamera.setPreviewCallback(previewCallback);

		mCamera.startPreview();
	}
	
	//http://stackoverflow.com/questions/2103368/color-logic-algorithm
	//http://stackoverflow.com/questions/9018016/how-to-compare-two-colors
	//http://geosoft.no/software/colorutil/ColorUtil.java.html
	
	public double getColorMatchPercentage(int r_new , int g_new , int b_new )
	{
		double d = Math.sqrt(Math.abs((r_new - rgb_old[0])^2+
				(g_new - rgb_old[1])
				^2+(b_new - rgb_old[2])^2));
		
		double p = d/Math.sqrt((255)^2+(255)^2+(255)^2);

		return p;

		
	}
	PreviewCallback previewCallback = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			if (data != null) {
				Log.e("", "onPreviewFrame pass");
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				Camera.Parameters parameters = camera.getParameters();
				Size size = parameters.getPreviewSize();
				YuvImage yuv = new YuvImage(data, ImageFormat.NV21, size.width,
						size.height, null);

				// bWidth and bHeight define the size of the bitmap you wish the
				// fill with the preview image
				yuv.compressToJpeg(new Rect(0, 0, size.width, size.height),
						100, out);
				byte[] bytes = out.toByteArray();
				Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0,
						bytes.length);
				Bitmap resizebitmap = Bitmap.createBitmap(bitmap,
						bitmap.getWidth() / 2, bitmap.getHeight() / 2, 50, 50);

				imageView.setImageBitmap(rotateImage(resizebitmap, 90));
				//if(isSet)
				setHexAndDominate(resizebitmap);
			}
		}
	};

	private void initMediaRecorder() {
		mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
		CamcorderProfile camcorderProfile_HQ = CamcorderProfile
				.get(CamcorderProfile.QUALITY_HIGH);
		mediaRecorder.setProfile(camcorderProfile_HQ);
		mediaRecorder.setOutputFile("/dev/null");
		mediaRecorder.setVideoFrameRate(2);

	}

	public Bitmap rotateImage(Bitmap src, float degree) {
		// create new matrix object
		Matrix matrix = new Matrix();
		// setup rotation degree
		matrix.postRotate(degree);
		// return new bitmap rotated using matrix
		return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(),
				matrix, true);
	}

	private void prepareMediaRecorder() {
		mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());

		try {
			mediaRecorder.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setHexAndDominate(Bitmap icon) {

		//int averageColor = getAverageColor(icon);
		//Log.i("Color Int", averageColor + "");

		//String averageHex = String.format("#%06X", 0xFFFFFF & averageColor);
		//Hex.setText(averageHex);
		//Hex.setBackgroundColor(averageColor);

		//
		final int[]rgb  = rgb_new = getMostCommonColor(icon);
		final int dominanteColor = Color.rgb(rgb[0], rgb[1], rgb[2]);
		
		this.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// parse dominate color
				String dominateHex = String.format("#%06X", 0xFFFFFF & dominanteColor);
				dominateColor.setText(dominateHex);	
				dominateColor.setBackgroundColor(dominanteColor);
				
				red_vlaue.setText(rgb[0]+"");
				green_value.setText(rgb[1]+"");
				blue_value.setText(rgb[2]+"");	
				
				if(isMatch)
				{
					double  result = 109 - (getColorMatchPercentage(rgb_new[0],rgb_new[1], rgb_new[2]) * 100);
					   result = (Double.isNaN(result)) ? 0.0 : (int)result;
					   if(result > 100)
						   result = 100;
				   percentage_txt.setText(result + " %");
				}

			}
		});
		
		
		//
		this.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				bar_red.setProgress((int) (rgb[0]/2.55));
				bar_green.setProgress((int) (rgb[1]/2.55));
				bar_blue.setProgress((int) (rgb[2]/2.55));
			}
		});
		
		//
		
		
		// common color
		//int commonColor = convert32Colorto2(dominanteColor);

		//colorName.setBackgroundColor(commonColor);
	}

	public int getAverageColor(Bitmap bitmap) {
		int redBucket = 0;
		int greenBucket = 0;
		int blueBucket = 0;
		int pixelCount = 0;

		for (int y = 0; y < bitmap.getHeight(); y++) {
			for (int x = 0; x < bitmap.getWidth(); x++) {
				int c = bitmap.getPixel(x, y);

				pixelCount++;
				redBucket += Color.red(c);
				greenBucket += Color.green(c);
				blueBucket += Color.blue(c);
				// does alpha matter?
			}
		}

		int averageColor = Color.rgb(redBucket / pixelCount, greenBucket
				/ pixelCount, blueBucket / pixelCount);
		return averageColor;
	}

	public static int[] getMostCommonColor(Bitmap bm/* , Activity activity */) {

		// Bitmap bm = ImageTester.getBitmapFromAsset(
		// activity.getApplicationContext(), "colors.bmp");

		int height = bm.getHeight();
		int width = bm.getWidth();

		Map<Integer, Integer> m = new HashMap<Integer, Integer>();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int rgb = bm.getPixel(i, j);
				// int[] rgbArr = getRGBArr(rgb);
				// Filter out grays....
				// if (!isGray(rgbArr)) {
				Integer counter = m.get(rgb);
				if (counter == null)
					counter = 0;
				counter++;
				m.put(rgb, counter);
				// }
			}
		}
		// String colourHex = getMostCommonColour(m);
		List<Object> list = new LinkedList<Object>(m.entrySet());

		Collections.sort(list, new Comparator<Object>() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue())
						.compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		Map.Entry me = (Map.Entry) list.get(list.size() - 1);
		int[] rgb = getRGBArr((Integer) me.getKey());
/*		Log.d("color in hex string ",
				Integer.toHexString(rgb[0]) + " " + Integer.toHexString(rgb[1])
						+ " " + Integer.toHexString(rgb[2]));*/
		Log.d("color in integer",(rgb[0]) + " " + (rgb[1]) + " " + (rgb[2]));
		//return Color.rgb(rgb[0], rgb[1], rgb[2]);
		// return getMostCommonColour(m);
		return rgb;
	}

	public static int[] getRGBArr(int pixel) {
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		return new int[] { red, green, blue };

	}

	int convert32Colorto2(int color) {
		int a, r, g, b;
		a = Color.alpha(color);
		r = Color.red(color);
		g = Color.green(color);
		b = Color.blue(color);
		if (r >> 7 == 1)
			r = 0xff;
		else
			r = 0x00;
		if (g >> 7 == 1)
			g = 0xff;
		else
			g = 0x00;
		if (b >> 7 == 1)
			b = 0xff;
		else
			b = 0x00;
		return (a << 24) | (r << 16) | (g << 8) | (b);
	}

	private void setBestPictureResolution() {
		// get biggest picture size
		final Parameters p = mCamera.getParameters();
		width = pref.getInt("Picture_Width", 0);
		height = pref.getInt("Picture_height", 0);

		if (width == 0 | height == 0) {
			pictureSize = getBiggestPictureSize(p);
			if (pictureSize != null)
				p.setPictureSize(pictureSize.width, pictureSize.height);
			// save width and height in sharedprefrences
			width = pictureSize.width;
			height = pictureSize.height;
			editor.putInt("Picture_Width", width);
			editor.putInt("Picture_height", height);
			editor.commit();

		} else {
			p.setPictureSize(width, height);
		}
	}

	private Camera.Size getBiggestPictureSize(Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPictureSizes()) {
			if (result == null) {
				result = size;
			} else {
				int resultArea = result.width * result.height;
				int newArea = size.width * size.height;

				if (newArea > resultArea) {
					result = size;
				}
			}
		}

		return (result);
	}
	
	@Override
	protected void onPause() {
		if (mCamera != null) {
		    mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		super.onPause();
	}
}